import { Route, Switch } from 'react-router-dom';

import imageLogo from '../assets/images/logo.png';

import { Header } from './Components/Header';
import { HomePage } from './Core/Pages/HomePage';
import { PreviewPage } from './Core/Pages/PreviewPage';
import './global.css';

export const App = (): JSX.Element => {
  return (
    <div className={'wrapper'}>
      <Header imageLogo={imageLogo} linksText={['Home', 'Preview']} linksPath={['/', '/preview']} />
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/preview" exact>
          <PreviewPage />
        </Route>
        <Route path="/preview/new" exact>
          <PreviewPage />
        </Route>
        <Route path="/preview/:id" exact>
          <PreviewPage />
        </Route>
      </Switch>
    </div>
  );
};
