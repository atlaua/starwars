import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IFiltersData } from '../../Core/Domain/interfaces/IFiltersData';
import { IHeroes } from '../../Core/Domain/interfaces/IHeroes';

import { IParametersModel } from './../../services/parameters/models/IParametersModel';

export interface IPreviewState {
  registryData: IHeroes[];
  currentPage: number;
  genderParameters: IParametersModel[];
  raceParameters: IParametersModel[];
  sideParameters: IParametersModel[];
  filtersData: IFiltersData;
  isHeroModalOpen: boolean;
  isAddNewHeroModalOpen: boolean;
}

const initialState: IPreviewState = {
  registryData: [],
  currentPage: 1,
  genderParameters: [],
  raceParameters: [],
  sideParameters: [],
  filtersData: {
    gender: [],
    race: [],
    side: [],
    search: [],
  },
  isHeroModalOpen: false,
  isAddNewHeroModalOpen: false,
};

export const previewSlice = createSlice({
  name: 'preview',
  initialState,
  reducers: {
    setRegistryData: (state, action: PayloadAction<IHeroes[]>) => {
      state.registryData = action.payload;
    },

    setCurrentPage: (state, action: PayloadAction<number>) => {
      state.currentPage = action.payload;
    },

    setFetchedGenderParameters: (state) => {
      state;
    },
    setGenderParameters: (state, action: PayloadAction<IParametersModel[]>) => {
      state.genderParameters = action.payload;
    },
    setFetchedRaceParameters: (state) => {
      state;
    },
    setRaceParameters: (state, action: PayloadAction<IParametersModel[]>) => {
      state.raceParameters = action.payload;
    },
    setFetchedSideParameters: (state) => {
      state;
    },
    setSideParameters: (state, action: PayloadAction<IParametersModel[]>) => {
      state.sideParameters = action.payload;
    },

    setFiltersData: (state, action: PayloadAction<IFiltersData>) => {
      state.filtersData = action.payload;
    },

    setIsHeroModalOpen: (state, action: PayloadAction<boolean>) => {
      state.isHeroModalOpen = action.payload;
    },
    setIsAddNewHeroModalOpen: (state, action: PayloadAction<boolean>) => {
      state.isAddNewHeroModalOpen = action.payload;
    },
  },
});
