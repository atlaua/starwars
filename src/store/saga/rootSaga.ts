import { all } from 'redux-saga/effects';

import watchGenderParameters from './parameters/genderParameters.saga';
import watchRaceParameters from './parameters/raceParameters.saga';
import watchSideParameters from './parameters/sideParameters.saga';

export default function* rootSaga(): Generator<unknown> {
  yield all([watchGenderParameters(), watchRaceParameters(), watchSideParameters()]);
}
