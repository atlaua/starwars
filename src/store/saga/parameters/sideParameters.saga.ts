import { all, call, put, takeLatest } from 'typed-redux-saga';

import { getParameters } from '../../../services/parameters/getParameters';
import { previewSlice } from '../../preview/previewSlice';

export function* getSideParametersSaga(): Generator<any, any, any> {
  const sideParameters = yield* call(getParameters.getSideParameters);

  yield put(previewSlice.actions.setSideParameters(sideParameters));
}

export default function* (): Generator<any, any, any> {
  yield all([takeLatest(previewSlice.actions.setFetchedGenderParameters, getSideParametersSaga)]);
}
