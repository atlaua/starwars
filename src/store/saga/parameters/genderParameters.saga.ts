import { all, call, put, takeLatest } from 'typed-redux-saga';

import { getParameters } from '../../../services/parameters/getParameters';
import { previewSlice } from '../../preview/previewSlice';

export function* getGenderParametersSaga(): Generator<any, any, any> {
  const genderParameters = yield* call(getParameters.getGenderParameters);

  yield put(previewSlice.actions.setGenderParameters(genderParameters));
}

export default function* (): Generator<any, any, any> {
  yield all([takeLatest(previewSlice.actions.setFetchedGenderParameters, getGenderParametersSaga)]);
}
