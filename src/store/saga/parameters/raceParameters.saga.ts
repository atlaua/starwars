import { all, call, put, takeLatest } from 'typed-redux-saga';

import { getParameters } from '../../../services/parameters/getParameters';
import { previewSlice } from '../../preview/previewSlice';

export function* getRaceParametersSaga(): Generator<any, any, any> {
  const raceParameters = yield* call(getParameters.getRaceParameters);

  yield put(previewSlice.actions.setRaceParameters(raceParameters));
}

export default function* (): Generator<any, any, any> {
  yield all([takeLatest(previewSlice.actions.setFetchedGenderParameters, getRaceParametersSaga)]);
}
