import { Form, Formik, FormikErrors } from 'formik';
import { useHistory } from 'react-router-dom';

import closeBtn from '../../../../assets/images/closeBtn.png';
import { ButtonComponent } from '../../../Components/ButtonComponent';
import { heroesContent } from '../../../services/heroes/heroesContent';
import { previewSlice } from '../../../store/preview/previewSlice';
import { useAppDispatch, useAppSelector } from '../../../store/store';
import { Colors } from '../../AddForm/Colors/Colors';
import { HeroCharacterization } from '../../AddForm/HeroCharacterization/HeroCharacterization';
import { HeroPhoto } from '../../AddForm/HeroPhoto/HeroPhoto';
import { mapAddHeroesModel } from '../../Domain/Factory/mapHeroes';
import { IAddForm } from '../../Domain/interfaces/IAddForm';

import { IFormErrors, validate } from './../../../utils/validators';
import { AddHeroPreview } from './AddHeroPreview/AddHeroPreview';
import './newHeroModal.css';

export const NewHeroModal = (): JSX.Element => {
  const filtersData = useAppSelector((state) => state.preview.filtersData);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const closeModalBtn = (): void => {
    if (Object.values(filtersData).filter((el: string[]) => el.length !== 0).length === 0) {
      history.replace('/preview');
      dispatch(previewSlice.actions.setIsAddNewHeroModalOpen(false));
    } else {
      history.replace(`/preview?filters=${JSON.stringify(filtersData)}`);
      dispatch(previewSlice.actions.setIsAddNewHeroModalOpen(false));
    }
  };

  const initialValues: IAddForm = {
    heroName: '',
    addGender: {
      id: '',
      value: '',
      isChecked: false,
    },
    addRace: {
      id: '',
      value: '',
      isChecked: false,
    },
    addSide: {
      id: '',
      value: '',
      isChecked: false,
    },
    addDescription: '',
    tags: '',
    photoInputFile: '',
    colorName: '#FFFFFF',
    backgroundColor: '#000000',
    parametersColor: '#FFFFFF',
  };

  return (
    <div id={'newHeroModal'} className={'newHeroModal'}>
      <img className={'modal__closeBtn'} src={closeBtn} alt={'закрыть'} onClick={(): void => closeModalBtn()} />

      <Formik
        initialValues={initialValues}
        onSubmit={(values): void => {
          heroesContent.addNewHero(mapAddHeroesModel(values));
          closeModalBtn();
          const getFilteredRegistryData = async (): Promise<void> => {
            const registryData = await heroesContent.getHeroes();
            dispatch(previewSlice.actions.setRegistryData(registryData));
          };
          void getFilteredRegistryData();
        }}
        validate={validate}
      >
        {(props): JSX.Element => {
          const {
            values,
            touched,
            errors,
            dirty,
            isSubmitting,
            setFieldValue,
            handleChange,
            handleBlur,
            handleSubmit,
          } = props;
          return (
            <div className={'wrapper_flexbox'} onClick={(e): void => e.stopPropagation()}>
              <Form onSubmit={handleSubmit}>
                <div className={'modalForm'}>
                  <HeroCharacterization
                    values={values}
                    errors={errors as FormikErrors<IFormErrors>}
                    touched={touched}
                    setFieldValue={setFieldValue}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                  />
                </div>
                <div className={'photoColorWrapper'}>
                  <HeroPhoto
                    values={values}
                    errors={errors as FormikErrors<IFormErrors>}
                    touched={touched}
                    setFieldValue={setFieldValue}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                  />
                  <Colors
                    values={values}
                    errors={errors as FormikErrors<IFormErrors>}
                    touched={touched}
                    setFieldValue={setFieldValue}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                  />
                </div>
                <div className={'saveFormBtn'}>
                  <ButtonComponent
                    text={'Save'}
                    buttonProps={{ style: { width: '122px', height: '55px' } }}
                    textProps={{ style: { fontSize: '18px' } }}
                    type={'submit'}
                    disabled={!dirty || isSubmitting}
                  />
                </div>
              </Form>
              <hr className={'separator'}></hr>
              <AddHeroPreview values={values} />
            </div>
          );
        }}
      </Formik>
    </div>
  );
};
