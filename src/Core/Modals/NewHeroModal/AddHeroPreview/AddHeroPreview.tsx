import { FC, useState } from 'react';

import arrowIcon from '../../../../../assets/images/arrowForward.png';
import { Arrow } from '../../../../Components/Arrow';
import { Card } from '../../../../Components/CardComponent';
import { CardDescription } from '../../../../Components/CardDescription';
import { Pagination } from '../../../../Components/Pagination';
import { PaginationDot } from '../../../../Components/PaginationDot';
import { mapAddHeroesModel } from '../../../Domain/Factory/mapHeroes';
import { IAddForm } from '../../../Domain/interfaces/IAddForm';
import { HeroModal } from '../../HeroModal/HeroModal';
import './addHeroPreview.css';

interface IAddHeroPreview {
  values: IAddForm;
}

export const AddHeroPreview: FC<IAddHeroPreview> = (props): JSX.Element => {
  const views: string[] = ['View1', 'View2'];
  const maxElementsToShow = 1;
  const [viewingWindow, setViewingWindow] = useState<number>(0);

  const onClickBackArrow = (): void => {
    viewingWindow > 0 ? setViewingWindow(viewingWindow - 1) : null;
  };
  const onClickForwardArrow = (): void => {
    viewingWindow >= 1 ? null : setViewingWindow(viewingWindow + 1);
  };

  return (
    <div className={'modalFormView'}>
      <div style={{ position: 'absolute', top: '45px' }}>Preview</div>
      <div className={'views'}>
        {views.map((el, index) => (
          <span key={index} style={viewingWindow === index ? { color: '#FFFFFF' } : { color: '#6B6B6B' }}>
            {el}
          </span>
        ))}
      </div>
      <div className={'viewsContainer'}>
        {views[viewingWindow] === 'View1' ? (
          <Card
            id={props.values.heroName}
            name={props.values.heroName}
            avatar={props.values.photoInputFile}
            heroNameProps={{ style: { color: props.values.colorName } }}
            avatarProps={{ style: { width: '254px', height: '198px' } }}
          >
            <CardDescription
              gender={props.values.addGender.value}
              race={props.values.addRace.value}
              side={props.values.addSide.value}
              fieldDescriptionChildProps={{ style: { color: props.values.parametersColor } }}
              descriptionProps={{ style: { fontSize: '13px' } }}
              wrapperProps={{ style: { backgroundColor: props.values.backgroundColor } }}
            />
          </Card>
        ) : (
          <HeroModal
            item={mapAddHeroesModel(props.values)}
            fontSizeCardDescription={'10px'}
            fontSizeHeroName={'25px'}
            fontSizeDescription={'10px'}
            fontSizeTags={'10px'}
            showCloseBtn={false}
          />
        )}
      </div>
      <div className={'paginationAddHero'}>
        <div className={'paginationAddHero__dots'}>
          <Pagination
            totalElements={views.length}
            shownElementsOnPage={maxElementsToShow}
            currentPage={viewingWindow + 1}
            onClickForwardArrow={onClickForwardArrow}
            onClickBackArrow={onClickBackArrow}
            backArrow={<Arrow isForwardArrow={false} arrowForwardIcon={arrowIcon} />}
            forwardArrow={<Arrow isForwardArrow={true} arrowForwardIcon={arrowIcon} />}
            paginationDot={<PaginationDot />}
          />
        </div>
      </div>
    </div>
  );
};
