import { FC } from 'react';
import { useHistory } from 'react-router-dom';

import { CardDescription } from '../../../Components/CardDescription';
import { IHeroCardModel } from '../../../services/heroes/models/IHeroCardModel';
import { previewSlice } from '../../../store/preview/previewSlice';
import { useAppDispatch, useAppSelector } from '../../../store/store';
import './heroModal.css';

interface IHeroModalProps {
  item: IHeroCardModel;
  fontSizeCardDescription: string;
  fontSizeHeroName: string;
  fontSizeDescription: string;
  fontSizeTags: string;
  showCloseBtn: boolean;
}

export const HeroModal: FC<IHeroModalProps> = (props): JSX.Element => {
  const tagsArray = [props.item.tag1, props.item.tag2, props.item.tag3];
  const tagColors = ['#9CC97B', '#C9897B', '#3FC4FD'];
  const filtersData = useAppSelector((state) => state.preview.filtersData);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const closeModalBtn = (): void => {
    if (Object.values(filtersData).filter((el: string[]) => el.length !== 0).length === 0) {
      history.replace('/preview');
      dispatch(previewSlice.actions.setIsHeroModalOpen(false));
    } else {
      history.replace(`/preview?filters=${JSON.stringify(filtersData)}`);
      dispatch(previewSlice.actions.setIsHeroModalOpen(false));
    }
  };

  return (
    <div
      className={'heroModal'}
      style={{ background: props.item?.backgroundColor }}
      onClick={(e): void => e.stopPropagation()}
    >
      <div className={'heroModal__information'}>
        <span className={'heroModal__name'} style={{ color: props.item.nameColor, fontSize: props.fontSizeHeroName }}>
          {props.item.name}
        </span>
        <div className={'heroModal__description'}>
          {props.item && (
            <CardDescription
              gender={props.item.gender ? props.item.gender.value : ''}
              race={props.item.race ? props.item.race.value : ''}
              side={props.item.side ? props.item.side.value : ''}
              fieldDescriptionChildProps={{ style: { color: props.item.parametersColor } }}
              descriptionProps={{ style: { fontSize: props.fontSizeCardDescription } }}
              wrapperProps={{ style: { backgroundColor: props.item.backgroundColor } }}
            />
          )}
        </div>
        <p style={{ lineHeight: '190%', fontSize: props.fontSizeDescription }}>{props.item?.description}</p>
      </div>
      <div className={'heroModal__img'}>
        {props.item.imageURL ? <img src={props.item.imageURL} /> : <div></div>}
        <div className={'modal__tags'}>
          {tagsArray.map((tag, index) => (
            <span
              key={index}
              style={{
                backgroundColor: tagColors[index],
                padding: tag ? '2%' : undefined,
                margin: '3%',
                borderRadius: 4,
                color: '#000000',
                fontSize: props.fontSizeTags,
              }}
            >
              {tag}
            </span>
          ))}
        </div>
      </div>
      {props.showCloseBtn ? (
        <div className={'modal__closeBtn'} onClick={(): void => closeModalBtn()}>
          <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
            <defs>
              <filter id="myfilter" x="0" y="0" width="200%" height="200%">
                <feOffset result="offOut" in="SourceAlpha" dx="1" dy="1" />
                <feGaussianBlur result="blurOut" in="offOut" stdDeviation="1" />
                <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
              </filter>
            </defs>
            <path
              filter="url(#myfilter)"
              d="M16.5 30.25C24.0939 30.25 30.25 24.0939 30.25 16.5C30.25 8.90608 24.0939 2.75 16.5 2.75C8.90608 2.75 2.75 8.90608 2.75 16.5C2.75 24.0939 8.90608 30.25 16.5 30.25Z"
              stroke="white"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              filter="url(#myfilter)"
              d="M20.625 12.375L12.375 20.625"
              stroke="white"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              filter="url(#myfilter)"
              d="M12.375 12.375L20.625 20.625"
              stroke="white"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        </div>
      ) : null}
    </div>
  );
};
