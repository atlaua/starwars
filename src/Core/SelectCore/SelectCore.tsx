import { FC, HTMLAttributes } from 'react';

import { Dropdown } from '../../Components/Dropdown';
import { Input } from '../../Components/InputComponent';
import { Select } from '../../Components/Select';

interface ISelectCoreProps {
  idWrapper: string;
  isDropdownOpen: boolean;
  setIsDropdownOpen: React.Dispatch<React.SetStateAction<boolean>>;
  dropdownChildren: React.ReactNode;
  dropdownId: string;
  inputId: string;
  inputValue: string;
  handleClickWrapper: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  inputName?: string;
  idElementForClick?: string;
  dropdownProps?: HTMLAttributes<HTMLDivElement>;
  inputProps?: HTMLAttributes<HTMLInputElement>;
  imageIconProps?: HTMLAttributes<HTMLDivElement>;
  iconImg?: string;
  label?: string;
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
  handleChange?: {
    (e: React.ChangeEvent<HTMLInputElement>): void;
    <T = string | React.ChangeEvent<HTMLInputElement>>(field: T): T extends React.ChangeEvent<HTMLInputElement>
      ? void
      : (e: string | React.ChangeEvent<HTMLInputElement>) => void;
  };
  handleBlur?: {
    (e: React.FocusEvent<HTMLInputElement, Element>): void;
    <T = HTMLInputElement>(fieldOrEvent: T): T extends string ? (e: HTMLInputElement) => void : void;
  };
}

export const SelectCore: FC<ISelectCoreProps> = (props): JSX.Element => {
  return (
    <div>
      <Select
        idWrapper={props.idWrapper}
        wrapperProps={props.wrapperProps}
        handleClickWrapper={props.handleClickWrapper}
        label={props.label}
        select={
          <Input
            id={props.inputId}
            name={props.inputName}
            value={props.inputValue}
            inputProps={props.inputProps}
            onChangeInput={props.handleChange}
            onBlurInput={props.handleBlur}
            icon={props.iconImg}
            disabled={true}
            imageIconProps={props.imageIconProps}
          />
        }
        dropdownId={props.dropdownId}
        isDropdownOpen={props.isDropdownOpen}
        setIsDropdownOpen={props.setIsDropdownOpen}
        dropdown={
          <Dropdown id={props.dropdownId} wrapperProps={{ ...props.dropdownProps }}>
            {props.dropdownChildren}
          </Dropdown>
        }
        idElementForClick={props.idElementForClick}
      />
    </div>
  );
};
