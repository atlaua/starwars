import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';

import { heroesContent } from '../../services/heroes/heroesContent';
import { previewSlice } from '../../store/preview/previewSlice';
import { useAppDispatch, useAppSelector } from '../../store/store';
import { IFiltersData } from '../Domain/interfaces/IFiltersData';
import { FormFilters } from '../FormFilters/FormFilters';
import { Registry } from '../Registry/Registry';
import './previewPage.css';

export const PreviewPage = (): JSX.Element => {
  const [initialize, setInitialize] = useState<boolean>(false);
  const filtersData = useAppSelector((state) => state.preview.filtersData);
  const dispatch = useAppDispatch();
  const history = useHistory();

  useEffect(() => {
    if (location.pathname === '/preview/new') {
      dispatch(previewSlice.actions.setIsAddNewHeroModalOpen(true));
    }
  }, []);

  useEffect(() => {
    const queryParams = new URLSearchParams(location.search);
    const queryFilters = queryParams.get('filters');

    if (queryFilters) {
      const parseQueryFilters = JSON.parse(queryFilters) as IFiltersData;
      dispatch(previewSlice.actions.setFiltersData(parseQueryFilters));
    }
  }, []);

  useEffect(() => {
    if (!initialize) {
      setInitialize(true);
    } else if (Object.values(filtersData).filter((el: string[]) => el.length !== 0).length === 0) {
      history.replace('/preview');
    } else {
      history.replace(`/preview?filters=${JSON.stringify(filtersData)}`);
    }
  }, [filtersData]);

  useEffect(() => {
    const getFilteredRegistryData = async (): Promise<void> => {
      const registryData = await heroesContent.getHeroes(filtersData);
      dispatch(previewSlice.actions.setRegistryData(registryData));
    };
    void getFilteredRegistryData();
  }, [filtersData]);

  useEffect(() => {
    dispatch(previewSlice.actions.setFetchedGenderParameters());
    dispatch(previewSlice.actions.setFetchedRaceParameters());
    dispatch(previewSlice.actions.setFetchedSideParameters());
  }, []);

  return (
    <div className={'previewPage__wrapper'}>
      <div className={'previewPage'}>
        <div className={'previewBanner'}>
          <h1>Who's Your Favorite Star Wars</h1>
          <FormFilters />
          <Registry />
        </div>
      </div>
      <div id="modal"></div>
    </div>
  );
};
