import { Link } from 'react-router-dom';

import { ButtonComponent } from '../../Components/ButtonComponent';

import './homePage.css';

export const HomePage = (): JSX.Element => {
  return (
    <div className={'homePage'}>
      <div className={'homePageContent'}>
        <div className="homePage__text">
          <h1>Find all your favorite heroes "Star Wars"</h1>
          <p>You can know the type of heroes, its strengths, disadvantages and abilities</p>
        </div>
        <Link to="/preview">
          <ButtonComponent
            text={'Start'}
            buttonProps={{ style: { width: '122px', height: '55px' } }}
            textProps={{ style: { fontSize: '18px' } }}
          />
        </Link>
      </div>
    </div>
  );
};
