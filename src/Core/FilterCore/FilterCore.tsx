import { FC, useState, useEffect } from 'react';

import arrow from '../../../assets/images/arrowDownSVG.svg';
import { FilterCheckboxes } from '../../Components/FilterCheckboxes';
import { ISelectContent } from '../Domain/interfaces/ISelectContent';
import { SelectCore } from '../SelectCore/SelectCore';

import { ICurrentFilterState } from './../FormFilters/FormFilters';
import './filterCore.css';

interface IFilterProps {
  filterName: string;
  dropdownHeight: number;
  initialValue: ISelectContent[];
  updateCheckboxState: (currentFilterState: ICurrentFilterState) => void;
}

export const FilterCore: FC<IFilterProps> = (props): JSX.Element => {
  const [initialize, setInitialize] = useState<boolean>(false);
  const [isFilterDropdownOpen, setIsFilterDropdownOpen] = useState(false);
  const [currentFilterState, setCurrentFilterState] = useState<ICurrentFilterState>({
    filterName: props.filterName.toLocaleLowerCase(),
    valueIds: [],
  });
  const [selectedFilter, setSelectedFilter] = useState<string>('');

  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
    setSelectedFilter(e.currentTarget.id);
    setIsFilterDropdownOpen(!isFilterDropdownOpen);
  };

  const checkedFilterCheckbox = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const isChecked = e.currentTarget.checked;
    const valueId = e.currentTarget.id;
    if (isChecked) {
      setCurrentFilterState((prev) => {
        return { ...prev, filterName: selectedFilter, valueIds: [...prev.valueIds, valueId] };
      });
    } else {
      setCurrentFilterState((prev) => {
        prev.valueIds = [...prev.valueIds.filter((el) => el !== valueId)];
        return { ...prev };
      });
    }
  };

  useEffect(() => {
    if (!initialize) {
      setInitialize(true);
    } else {
      props.updateCheckboxState(currentFilterState);
    }
  }, [currentFilterState]);

  useEffect(() => {
    props.initialValue.map((el) => {
      if (el.isChecked) {
        setCurrentFilterState((prev) => {
          return { ...prev, valueIds: [...prev.valueIds, el.id] };
        });
      }
    });
  }, []);

  return (
    <div>
      <SelectCore
        idWrapper={props.filterName.toLowerCase()}
        wrapperProps={{ style: { marginRight: '12px', position: 'relative' } }}
        handleClickWrapper={(e): void => handleClick(e)}
        inputId={props.filterName.toLowerCase()}
        inputValue={
          props.initialValue.filter((value) => value.isChecked === true).length
            ? `${props.filterName}: ${props.initialValue.filter((value) => value.isChecked === true).length}`
            : `${props.filterName}`
        }
        inputProps={{ style: { width: '140px', height: '45px' } }}
        dropdownId={props.filterName.toLowerCase()}
        isDropdownOpen={isFilterDropdownOpen}
        setIsDropdownOpen={setIsFilterDropdownOpen}
        dropdownChildren={
          <div className={'dropdown_filter'}>
            {props.initialValue.map((el) => (
              <FilterCheckboxes
                key={`${el.id}`}
                id={`${el.id}`}
                inputName={el.value}
                label={`${el.value}`}
                isChecked={el.isChecked}
                checkedInput={checkedFilterCheckbox}
              />
            ))}
          </div>
        }
        dropdownProps={{ style: { width: '110px', height: `${props.dropdownHeight}px` } }}
        iconImg={arrow}
        imageIconProps={{ style: { top: '19px', left: '115px' } }}
      />
    </div>
  );
};
