import { FC, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { Card } from '../../Components/CardComponent';
import { CardDescription } from '../../Components/CardDescription';
import { Modal } from '../../Components/Modal';
import { heroesContent } from '../../services/heroes/heroesContent';
import { IHeroCardModel } from '../../services/heroes/models/IHeroCardModel';
import { previewSlice } from '../../store/preview/previewSlice';
import { useAppSelector, useAppDispatch } from '../../store/store';
import { IHeroes } from '../Domain/interfaces/IHeroes';
import { HeroModal } from '../Modals/HeroModal/HeroModal';
import './list.css';

interface ICardsListProps {
  currentPage: number;
  heroesMap: Map<number, IHeroes[]>;
}

export const CardsList: FC<ICardsListProps> = (props): JSX.Element => {
  const { id: queryId } = useParams<{ id?: string }>();
  const [openCard, setOpenCard] = useState<IHeroCardModel>({});
  const isHeroModalOpen = useAppSelector((state) => state.preview.isHeroModalOpen);
  const filtersData = useAppSelector((state) => state.preview.filtersData);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const getCardHero = async (id: string): Promise<void> => {
    const openCard = await heroesContent.getHeroCard(id);
    setOpenCard(openCard as unknown as IHeroCardModel);
  };

  useEffect(() => {
    if (queryId) {
      dispatch(previewSlice.actions.setIsHeroModalOpen(true));
      void getCardHero(queryId);
    }
  }, [queryId]);

  const openModal = (id: string): void => {
    history.replace(`/preview/${id}`);
    void getCardHero(id);
  };

  const closeModal = (): void => {
    if (Object.values(filtersData).filter((el: string[]) => el.length !== 0).length === 0) {
      history.replace('/preview');
      dispatch(previewSlice.actions.setIsHeroModalOpen(false));
    } else {
      history.replace(`/preview?filters=${JSON.stringify(filtersData)}`);
      dispatch(previewSlice.actions.setIsHeroModalOpen(false));
    }
  };

  return (
    <>
      <div className={'cardsList'}>
        {props.heroesMap.size !== 0 ? (
          props.heroesMap.get(props.currentPage)?.map((el, index) => (
            <Card
              key={index}
              handleClick={openModal}
              id={el.id}
              name={el.name}
              avatar={el.imageURL}
              heroNameProps={{ style: { color: el.colorName, fontSize: '24px' } }}
            >
              <CardDescription
                gender={el.gender}
                race={el.race}
                side={el.side}
                fieldDescriptionChildProps={{ style: { color: el.parametersColor } }}
                descriptionProps={{ style: { fontSize: '18px' } }}
                wrapperProps={{ style: { backgroundColor: el.backgroundColor } }}
              />
            </Card>
          ))
        ) : (
          <div className={'cardsList_noMatches'}>Sorry, no matches found</div>
        )}
      </div>
      {isHeroModalOpen && (
        <Modal closeModal={closeModal}>
          <HeroModal
            item={openCard}
            fontSizeCardDescription={'24px'}
            fontSizeHeroName={'50px'}
            fontSizeDescription={'20px'}
            fontSizeTags={'20px'}
            showCloseBtn={true}
          />
        </Modal>
      )}
    </>
  );
};
