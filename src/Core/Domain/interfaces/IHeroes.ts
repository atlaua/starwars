export interface IHeroes {
  id: string;
  name: string;
  imageURL: string;
  gender: string;
  race: string;
  side: string;
  colorName: string;
  backgroundColor: string;
  parametersColor: string;
}
