export interface ISelectContent {
  id: string;
  value: string;
  isChecked: boolean;
}
