import { ISelectContent } from './ISelectContent';

export interface IAddForm {
  heroName: string;
  addGender: ISelectContent;
  addRace: ISelectContent;
  addSide: ISelectContent;
  addDescription: string;
  tags: string;
  photoInputFile: string;
  colorName: string;
  backgroundColor: string;
  parametersColor: string;
}
