import { FormikErrors, FormikTouched } from 'formik';

import { IFormErrors } from '../../../utils/validators';

import { IAddForm } from './IAddForm';
import { ISelectContent } from './ISelectContent';

export interface IAddFormChildrenProps {
  values: IAddForm;
  errors: FormikErrors<IFormErrors>;
  touched: FormikTouched<IAddForm>;
  setFieldValue: (field: string, value: ISelectContent | string, shouldValidate?: boolean | undefined) => void;
  handleChange: {
    (e: React.ChangeEvent<HTMLInputElement>): void;
    <T = string | React.ChangeEvent<HTMLInputElement>>(field: T): T extends React.ChangeEvent<HTMLInputElement>
      ? void
      : (e: string | React.ChangeEvent<HTMLInputElement>) => void;
  };
  handleBlur: {
    (e: React.FocusEvent<HTMLInputElement, Element>): void;
    <T = HTMLInputElement>(fieldOrEvent: T): T extends string ? (e: HTMLInputElement) => void : void;
  };
}
