export interface IFiltersData {
  gender: string[];
  race: string[];
  side: string[];
  search: string[];
}
