import { IHeroCardModel } from '../../../services/heroes/models/IHeroCardModel';
import { IHeroesModel } from '../../../services/heroes/models/IHeroesModel';
import { IAddForm } from '../interfaces/IAddForm';
import { IHeroes } from '../interfaces/IHeroes';

export const mapHeroesModel = (model: IHeroesModel): IHeroes => ({
  id: model.id,
  name: model.name,
  imageURL: model.imageURL,
  gender: model.gender,
  race: model.race,
  side: model.side,
  colorName: model.nameColor,
  backgroundColor: model.backgroundColor,
  parametersColor: model.parametersColor,
});

export const mapAddHeroesModel = (model: IAddForm): IHeroCardModel => {
  return {
    name: model.heroName,
    description: model.addDescription,
    imageURL: model.photoInputFile,
    nameColor: model.colorName,
    backgroundColor: model.backgroundColor,
    parametersColor: model.parametersColor,
    tag1: model.tags.split(',')[0],
    tag2: model.tags.split(',')[1],
    tag3: model.tags.split(',')[2],
    gender: {
      id: model.addGender.id,
      value: model.addGender.value,
    },
    race: {
      id: model.addRace.id,
      value: model.addRace.value,
    },
    side: {
      id: model.addSide.id,
      value: model.addSide.value,
    },
  };
};
