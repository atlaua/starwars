import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';

import addIcon from '../../../assets/images/addPlus.png';
import searchIcon from '../../../assets/images/search-icon.png';
import { Input } from '../../Components/InputComponent';
import { Modal } from '../../Components/Modal';
import { previewSlice } from '../../store/preview/previewSlice';
import { useAppDispatch, useAppSelector } from '../../store/store';
import { addCurrentFilterCheckboxState } from '../../utils/addFiltersCheckboxesState';
import { IFiltersData } from '../Domain/interfaces/IFiltersData';
import { ISelectContent } from '../Domain/interfaces/ISelectContent';
import { FilterCore } from '../FilterCore/FilterCore';
import { NewHeroModal } from '../Modals/NewHeroModal/NewHeroModal';
import './form.css';

export interface ICurrentFilterState {
  filterName: string;
  valueIds: string[];
}

export const FormFilters = (): JSX.Element => {
  const [initialize, setInitialize] = useState<boolean>(false);
  const genderValues = useAppSelector((state) => state.preview.genderParameters);
  const raceValues = useAppSelector((state) => state.preview.raceParameters);
  const sideValues = useAppSelector((state) => state.preview.sideParameters);
  const filtersData = useAppSelector((state) => state.preview.filtersData);
  const [formFiltersState, setFormFiltersState] = useState<IFiltersData>(filtersData);
  const isAddNewHeroModalOpen = useAppSelector((state) => state.preview.isAddNewHeroModalOpen);
  const dispatch = useAppDispatch();
  const history = useHistory();

  const initialGender: ISelectContent[] = addCurrentFilterCheckboxState(genderValues, filtersData.gender);
  const initialRace: ISelectContent[] = addCurrentFilterCheckboxState(raceValues, filtersData.race);
  const initialSide: ISelectContent[] = addCurrentFilterCheckboxState(sideValues, filtersData.side);

  const updateCheckboxState = (currentFilterState: ICurrentFilterState): void => {
    if (currentFilterState.filterName === 'gender') {
      setFormFiltersState((prev) => {
        return { ...prev, search: [...filtersData.search], gender: currentFilterState.valueIds };
      });
    } else if (currentFilterState.filterName === 'race') {
      setFormFiltersState((prev) => {
        return { ...prev, search: [...filtersData.search], race: currentFilterState.valueIds };
      });
    } else if (currentFilterState.filterName === 'side') {
      setFormFiltersState((prev) => {
        return { ...prev, search: [...filtersData.search], side: currentFilterState.valueIds };
      });
    }
  };

  const searchInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setFormFiltersState((prev) => {
      if (e.currentTarget.value !== '') {
        return { ...prev, search: e.currentTarget.value.split(' ') };
      } else {
        return { ...prev, search: [] };
      }
    });
  };

  const searchValue = (): string => {
    if (filtersData.search.length > 0) {
      return filtersData.search.join(' ');
    } else {
      return '';
    }
  };

  useEffect(() => {
    if (!initialize) {
      setInitialize(true);
    } else {
      dispatch(previewSlice.actions.setFiltersData(Object.assign({}, formFiltersState)));
      dispatch(previewSlice.actions.setCurrentPage(1));
    }
  }, [formFiltersState]);

  const openModal = (): void => {
    history.replace('/preview/new');
    dispatch(previewSlice.actions.setIsAddNewHeroModalOpen(true));
  };

  const closeModal = (): void => {
    if (Object.values(filtersData).filter((el: string[]) => el.length !== 0).length === 0) {
      history.replace('/preview');
      dispatch(previewSlice.actions.setIsAddNewHeroModalOpen(false));
    } else {
      history.replace(`/preview?filters=${JSON.stringify(filtersData)}`);
      dispatch(previewSlice.actions.setIsAddNewHeroModalOpen(false));
    }
  };

  return (
    <form className={'form'}>
      <div className={'form__search'}>
        <Input
          id={'search'}
          inputProps={{ style: { height: '50px' } }}
          imageIconProps={{ style: { top: '14px', right: '17px' } }}
          placeholder={'Search heroes'}
          value={searchValue()}
          icon={searchIcon}
          iconAlt={'search_icon'}
          onChangeInput={searchInput}
        />
      </div>
      <div className={'form__FiltersAddWrapper'}>
        <div className={'formFilters__container'}>
          {initialGender.length !== 0 && (
            <FilterCore
              filterName={'Gender'}
              dropdownHeight={140}
              initialValue={initialGender}
              updateCheckboxState={updateCheckboxState}
            />
          )}
          {initialRace.length !== 0 && (
            <FilterCore
              filterName={'Race'}
              dropdownHeight={170}
              initialValue={initialRace}
              updateCheckboxState={updateCheckboxState}
            />
          )}
          {initialSide.length !== 0 && (
            <FilterCore
              filterName={'Side'}
              dropdownHeight={140}
              initialValue={initialSide}
              updateCheckboxState={updateCheckboxState}
            />
          )}
        </div>
        <div className={'form__addNewHero'} onClick={openModal}>
          <Input
            id={'addHero'}
            inputProps={{ style: { width: '140px', height: '45px' } }}
            imageIconProps={{ style: { top: '17px', right: '12px' } }}
            value={'Add'}
            disabled={true}
            icon={addIcon}
          />
        </div>
        {isAddNewHeroModalOpen && (
          <Modal closeModal={closeModal}>
            <NewHeroModal />
          </Modal>
        )}
      </div>
    </form>
  );
};
