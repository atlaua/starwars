import arrowIcon from '../../../assets/images/arrowForward.png';
import { Arrow } from '../../Components/Arrow';
import { Pagination } from '../../Components/Pagination';
import { PaginationDot } from '../../Components/PaginationDot';
import { previewSlice } from '../../store/preview/previewSlice';
import { useAppDispatch, useAppSelector } from '../../store/store';
import { getMappedHeroes } from '../../utils/pagedHeroes';
import { CardsList } from '../List/List';
import './registry.css';

export const Registry = (): JSX.Element => {
  const currentPage = useAppSelector((state) => state.preview.currentPage);
  const registryData = useAppSelector((state) => state.preview.registryData);
  const dispatch = useAppDispatch();
  const maxElementsToShow = 1;

  const heroesMap = getMappedHeroes(registryData);

  const onClickBackArrow = (): void => {
    currentPage >= 2 ? dispatch(previewSlice.actions.setCurrentPage(currentPage - 1)) : null;
  };

  const onClickForwardArrow = (): void => {
    currentPage >= heroesMap.size ? null : dispatch(previewSlice.actions.setCurrentPage(currentPage + 1));
  };

  return (
    <div className={'registry'}>
      <CardsList currentPage={currentPage} heroesMap={heroesMap} />
      <Pagination
        totalElements={heroesMap.size}
        shownElementsOnPage={maxElementsToShow}
        currentPage={currentPage}
        onClickForwardArrow={onClickForwardArrow}
        onClickBackArrow={onClickBackArrow}
        backArrow={<Arrow isForwardArrow={false} arrowForwardIcon={arrowIcon} />}
        forwardArrow={<Arrow isForwardArrow={true} arrowForwardIcon={arrowIcon} />}
        paginationDot={<PaginationDot />}
      />
    </div>
  );
};
