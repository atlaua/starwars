import { FC, useState } from 'react';

import { Input } from '../../../Components/InputComponent';
import { IAddFormChildrenProps } from '../../Domain/interfaces/IAddFormChildrenProps';
import './heroPhoto.css';

export const HeroPhoto: FC<IAddFormChildrenProps> = (props) => {
  const [isShow, setIsShow] = useState<string>('');
  const handleClick = (): void => {
    setIsShow('none');
    const photoInput = document.getElementById('photoInputFile');
    photoInput?.focus();
  };
  return (
    <div className={'form__addPhoto'}>
      <div className={'form__photoContainer'}>
        <div className={'form__uploadedPhotoContainer'}>
          {props.values.photoInputFile ? <img src={props.values.photoInputFile}></img> : null}
        </div>
        <div className={'form__photoInputHidden'} onClick={handleClick}>
          <span className={'form__photoInputFake_text'} style={{ display: isShow }}>
            Upload by URL
          </span>
          <Input
            id={'photoInputFile'}
            name={'photoInputFile'}
            value={props.values.photoInputFile}
            onChangeInput={props.handleChange}
            onBlurInput={props.handleBlur}
            inputProps={{ style: { borderColor: '#3fc4fd' } }}
            inputWrapperProps={{ style: { height: '32px' } }}
          />
        </div>
      </div>
      <div style={{ height: '15px' }}>
        {props.errors.photoInputFile && props.touched.photoInputFile && (
          <div className={'errors'}>{props.errors.photoInputFile}</div>
        )}
      </div>
    </div>
  );
};
