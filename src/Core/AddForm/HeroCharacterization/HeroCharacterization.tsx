import { FC } from 'react';

import { Input } from '../../../Components/InputComponent';

import { IAddFormChildrenProps } from '../../Domain/interfaces/IAddFormChildrenProps';

import { AddParameters } from './AddParameters/AddParameters';
import './heroCharacterization.css';

export const HeroCharacterization: FC<IAddFormChildrenProps> = (props): JSX.Element => {
  const re = /\s*,\s*/;

  return (
    <div className={'form__heroCharacterization'}>
      <div className={'form__addName'}>
        <label>Add name</label>
        <Input
          id={'heroName'}
          name={'heroName'}
          value={props.values.heroName}
          inputWrapperProps={{ style: { width: '445px', height: '30px' } }}
          onChangeInput={props.handleChange}
          onBlurInput={props.handleBlur}
        />
        <div style={{ height: '15px' }}>
          {props.errors.heroName && props.touched.heroName && <div className={'errors'}>{props.errors.heroName}</div>}
        </div>
      </div>
      <AddParameters idRace={'addRace'} idSide={'addSide'} idGender={'addGender'} {...props} />
      <div className={'form__description'}>
        <label className={'form__counter'}>
          <span>Add description</span>
          <span className={'form__counterColor'}>
            {props.values.addDescription.length ? props.values.addDescription.length : 0}/100
          </span>
        </label>
        <textarea
          className={'textarea'}
          id="addDescription"
          name="addDescription"
          value={props.values.addDescription}
          rows={6}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
        ></textarea>
        <div style={{ height: '15px' }}>
          {props.errors.addDescription && props.touched.addDescription && (
            <div className={'errors'}>{props.errors.addDescription}</div>
          )}
        </div>
      </div>
      <div className={'form__tags'}>
        <label className={'form__counter'}>
          <span>Add tags</span>
          <span className={'form__counterColor'}>
            {props.values.tags.trim() ? props.values.tags.split(re).length : 0}/3
          </span>
        </label>
        <Input
          id={'tags'}
          name={'tags'}
          value={props.values.tags}
          inputWrapperProps={{ style: { height: '32px' } }}
          onChangeInput={props.handleChange}
          onBlurInput={props.handleBlur}
        />
        <div style={{ height: '15px' }}>
          {props.errors.tags && props.touched.tags && <div className={'errors'}>{props.errors.tags}</div>}
        </div>
      </div>
    </div>
  );
};
