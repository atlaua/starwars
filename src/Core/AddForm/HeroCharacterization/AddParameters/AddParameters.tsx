import { FC, useState } from 'react';

import { FilterCheckboxes } from '../../../../Components/FilterCheckboxes';
import { useAppSelector } from '../../../../store/store';
import { IAddFormChildrenProps } from '../../../Domain/interfaces/IAddFormChildrenProps';
import { SelectCore } from '../../../SelectCore/SelectCore';
import './addParameters.css';

export const AddParameters: FC<IAddFormChildrenProps & { idGender: string; idRace: string; idSide: string }> = (
  props
): JSX.Element => {
  const [isGenderOpen, setIsGenderOpen] = useState<boolean>(false);
  const [isRaceOpen, setIsRaceOpen] = useState<boolean>(false);
  const [isSideOpen, setIsSideOpen] = useState<boolean>(false);
  const genderValues = useAppSelector((state) => state.preview.genderParameters);
  const raceValues = useAppSelector((state) => state.preview.raceParameters);
  const sideValues = useAppSelector((state) => state.preview.sideParameters);
  const [selectedInput, setSelectedInput] = useState<string>('');

  const handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
    if (e.currentTarget.id === props.idGender) {
      setIsGenderOpen(!isGenderOpen);
    } else if (e.currentTarget.id === props.idRace) {
      setIsRaceOpen(!isRaceOpen);
    } else {
      setIsSideOpen(!isSideOpen);
    }
    setSelectedInput(e.currentTarget.id);
  };

  const getCheckboxState = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const obj = {
      id: e.currentTarget.id,
      value: e.currentTarget.name,
      isChecked: e.currentTarget.checked,
    };
    if (e.currentTarget.checked) {
      if (selectedInput === props.idGender) {
        props.setFieldValue(props.idGender, obj);
        setIsGenderOpen(false);
      } else if (selectedInput === props.idRace) {
        props.setFieldValue(props.idRace, obj);
        setIsRaceOpen(false);
      } else if (selectedInput === props.idSide) {
        props.setFieldValue(props.idSide, obj);
        setIsSideOpen(false);
      }
    } else {
      if (selectedInput === props.idGender) {
        props.setFieldValue(props.idGender, { id: '', value: '', isChecked: false });
      } else if (selectedInput === props.idRace) {
        props.setFieldValue(props.idRace, { id: '', value: '', isChecked: false });
      } else if (selectedInput === props.idSide) {
        props.setFieldValue(props.idSide, { id: '', value: '', isChecked: false });
      }
    }
  };

  return (
    <div className={'form__parameters'}>
      <div className={'form__parameters_gender'}>
        <SelectCore
          label={'Gender'}
          idWrapper={props.idGender}
          handleClickWrapper={(e): void => handleClick(e)}
          inputId={props.idGender}
          inputName={'addGender'}
          inputValue={props.values.addGender.value}
          inputProps={{ style: { height: '30px' } }}
          dropdownId={props.idGender}
          isDropdownOpen={isGenderOpen}
          setIsDropdownOpen={setIsGenderOpen}
          dropdownChildren={genderValues.map((el) => (
            <FilterCheckboxes
              key={el.id}
              id={el.id}
              inputName={el.value}
              label={el.value}
              isChecked={el.id === props.values.addGender.id ? props.values.addGender.isChecked : false}
              checkedInput={getCheckboxState}
            />
          ))}
          dropdownProps={{ style: { width: '100px' } }}
          idElementForClick={'newHeroModal'}
        />
        <div style={{ height: '15px' }}>
          {props.errors.addGender && <div className={'errors'}>{props.errors.addGender}</div>}
        </div>
      </div>
      <div className={'form__parameters_race'}>
        <SelectCore
          label={'Race'}
          idWrapper={props.idRace}
          handleClickWrapper={(e): void => handleClick(e)}
          inputId={props.idRace}
          inputName={'addRace'}
          inputValue={props.values.addRace.value}
          inputProps={{ style: { height: '30px' } }}
          dropdownId={props.idRace}
          isDropdownOpen={isRaceOpen}
          setIsDropdownOpen={setIsRaceOpen}
          dropdownChildren={raceValues.map((el) => (
            <FilterCheckboxes
              key={el.id}
              id={el.id}
              inputName={el.value}
              label={el.value}
              isChecked={el.id === props.values.addRace.id ? props.values.addRace.isChecked : false}
              checkedInput={getCheckboxState}
            />
          ))}
          dropdownProps={{ style: { width: '100px' } }}
          idElementForClick={'newHeroModal'}
        />
        {props.errors.addRace && <div className={'errors'}>{props.errors.addRace}</div>}
      </div>
      <div className={'form__parameters_side'}>
        <SelectCore
          label={'Side'}
          idWrapper={props.idSide}
          handleClickWrapper={(e): void => handleClick(e)}
          inputId={props.idSide}
          inputName={'addRace'}
          inputValue={props.values.addSide.value}
          inputProps={{ style: { height: '30px' } }}
          dropdownId={props.idSide}
          isDropdownOpen={isSideOpen}
          setIsDropdownOpen={setIsSideOpen}
          dropdownChildren={sideValues.map((el) => (
            <FilterCheckboxes
              key={el.id}
              id={el.id}
              inputName={el.value}
              label={el.value}
              isChecked={el.id === props.values.addSide.id ? props.values.addSide.isChecked : false}
              checkedInput={getCheckboxState}
            />
          ))}
          dropdownProps={{ style: { width: '100px' } }}
          idElementForClick={'newHeroModal'}
        />
        {props.errors.addSide && <div className={'errors'}>{props.errors.addSide}</div>}
      </div>
    </div>
  );
};
