import { FC } from 'react';

import { InputColor } from '../../../Components/InputColor';
import { IAddFormChildrenProps } from '../../Domain/interfaces/IAddFormChildrenProps';
import './colors.css';

export const Colors: FC<IAddFormChildrenProps> = (props): JSX.Element => {
  return (
    <div className={'colors'}>
      <div className={'colors__flexbox'}>
        <InputColor id={'colorName'} value={props.values.colorName} onChangeInput={props.handleChange} />
        <label>Name color</label>
      </div>
      <div className={'colors__flexbox'}>
        <InputColor id={'backgroundColor'} value={props.values.backgroundColor} onChangeInput={props.handleChange} />
        <label>
          Background color <br /> of the parameters
        </label>
      </div>
      <div className={'colors__flexbox'}>
        <InputColor id={'parametersColor'} value={props.values.parametersColor} onChangeInput={props.handleChange} />
        <label>Color of parameters</label>
      </div>
    </div>
  );
};
