import axios from 'axios';

import { mapHeroesModel } from '../../Core/Domain/Factory/mapHeroes';
import { IFiltersData } from '../../Core/Domain/interfaces/IFiltersData';
import { IHeroes } from '../../Core/Domain/interfaces/IHeroes';

import { IDataResponse } from './models/IDataResponse';
import { IHeroCardModel } from './models/IHeroCardModel';

export const heroesContent = {
  getHeroes(filtersData?: IFiltersData): Promise<IHeroes[]> {
    if (filtersData && Object.values(filtersData).filter((el: string[]) => el.length !== 0).length !== 0) {
      return axios
        .get<IDataResponse>(
          `http://localhost:5000/api/STAR_WARS/character?values=${filtersData.search.join(
            '&values='
          )}&gender=${filtersData.gender.join('&gender=')}&race=${filtersData.race.join(
            '&race='
          )}&side=${filtersData.side.join('&side=')}&page=0&size=15`
        )
        .then((response) => {
          return response.data.content.map((el) => mapHeroesModel(el));
        });
    } else {
      return axios
        .get<IDataResponse>('http://localhost:5000/api/STAR_WARS/character?page=0&size=15')
        .then((response) => {
          return response.data.content.map((el) => mapHeroesModel(el));
        });
    }
  },

  getHeroCard(id: string): Promise<void | IHeroCardModel> {
    return axios.get<IHeroCardModel>(`http://localhost:5000/api/STAR_WARS/character/${id}`).then((response) => {
      return response.data;
    });
  },

  addNewHero(addHeroModel: IHeroCardModel): void {
    axios
      .post('http://localhost:5000/api/STAR_WARS/character', JSON.stringify(addHeroModel), {
        headers: {
          'content-type': 'application/json',
        },
      })
      .catch((err) => console.log(err));
  },
};
