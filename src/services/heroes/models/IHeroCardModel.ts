import { IParametersModel } from '../../parameters/models/IParametersModel';

export interface IHeroCardModel {
  id?: string;
  name?: string;
  description?: string;
  imageURL?: string;
  nameColor?: string;
  backgroundColor?: string;
  parametersColor?: string;
  tag1?: string;
  tag2?: string;
  tag3?: string;
  gender?: IParametersModel;
  race?: IParametersModel;
  side?: IParametersModel;
}
