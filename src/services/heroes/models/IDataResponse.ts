import { IHeroesModel } from './IHeroesModel';

export interface IDataResponse {
  content: IHeroesModel[];
  first: true;
  last: true;
  number: number;
  numberOfElements: number;
  size: number;
  totalElements: number;
  totalPages: number;
}
