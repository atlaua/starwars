import axios from 'axios';

import { IParametersModel } from './models/IParametersModel';

export const getParameters = {
  getGenderParameters(): Promise<IParametersModel[]> {
    return axios.get<IParametersModel[]>('http://localhost:5000/api/STAR_WARS/gender').then((response) => {
      return response.data;
    });
  },

  getRaceParameters(): Promise<IParametersModel[]> {
    return axios.get<IParametersModel[]>('http://localhost:5000/api/STAR_WARS/race').then((response) => {
      return response.data;
    });
  },

  getSideParameters(): Promise<IParametersModel[]> {
    return axios.get<IParametersModel[]>('http://localhost:5000/api/STAR_WARS/side').then((response) => {
      return response.data;
    });
  },
};
