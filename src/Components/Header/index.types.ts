import { HTMLAttributes } from 'react';

export interface IHeaderProps {
  imageLogo?: string;
  linksText: string[];
  linksPath: string[];
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
  imgProps?: HTMLAttributes<HTMLImageElement>;
}
