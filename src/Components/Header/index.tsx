import { FC } from 'react';

import { Navbar } from './Navbar';
import './header.css';
import { IHeaderProps } from './index.types';

export const Header: FC<IHeaderProps> = (props): JSX.Element => {
  return (
    <div {...props.wrapperProps} style={{ ...props.wrapperProps?.style }} className={'header'}>
      {props.imageLogo ? <img src={props.imageLogo} {...props.imgProps} style={{ ...props.imgProps?.style }} /> : null}
      <Navbar linksText={props.linksText} linksPath={props.linksPath} />
    </div>
  );
};
