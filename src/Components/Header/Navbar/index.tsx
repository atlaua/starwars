import { FC } from 'react';
import { NavLink } from 'react-router-dom';

import { INavbarProps } from './index.types';
import './navbar.css';

export const Navbar: FC<INavbarProps> = (props): JSX.Element => {
  return (
    <nav {...props.navWrapperProps} style={{ ...props.navWrapperProps?.style }} className={'navbar__list'}>
      {props.linksText.map((link, index) => (
        <div
          key={link}
          {...props.linkWrapperProps}
          style={{ ...props.linkWrapperProps?.style }}
          className={'navbar__listItem'}
        >
          <NavLink to={`${props.linksPath[index]}`}>{link}</NavLink>
        </div>
      ))}
    </nav>
  );
};
