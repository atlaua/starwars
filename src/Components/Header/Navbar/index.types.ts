import { HTMLAttributes } from 'react';

export interface INavbarProps {
  linksText: string[];
  linksPath: string[];
  navWrapperProps?: HTMLAttributes<HTMLElement>;
  linkWrapperProps?: HTMLAttributes<HTMLDivElement>;
}
