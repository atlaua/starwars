import { FC } from 'react';

import { IDropdownProps } from './index.types';
import './dropdown.css';

export const Dropdown: FC<IDropdownProps> = (props): JSX.Element => {
  return (
    <div
      id={`dropdown_${props.id}`}
      {...props.wrapperProps}
      className={'dropdown'}
      style={{ ...props.wrapperProps?.style }}
    >
      {props.children}
    </div>
  );
};
