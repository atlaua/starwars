import { HTMLAttributes } from 'react';

export interface IDropdownProps {
  id: string;
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
}
