import { FC } from 'react';

import { ICardDescriptionProps } from './index.types';
import './cardDescription.css';

export const CardDescription: FC<ICardDescriptionProps> = (props): JSX.Element => {
  return (
    <div {...props.wrapperProps} style={{ ...props.wrapperProps?.style }}>
      <div {...props.descriptionProps} className={'description'} style={{ ...props.descriptionProps?.style }}>
        <div {...props.fieldDescriptionProps} style={{ ...props.fieldDescriptionProps?.style }} className={'field'}>
          <span style={{ ...props.fieldDescriptionChildProps?.style }}>Gender</span>
          <span>{props.gender}</span>
        </div>
        <div {...props.fieldDescriptionProps} style={{ ...props.fieldDescriptionProps?.style }} className={'field'}>
          <span style={{ ...props.fieldDescriptionChildProps?.style }}>Race</span>
          <span>{props.race}</span>
        </div>
        <div {...props.fieldDescriptionProps} style={{ ...props.fieldDescriptionProps?.style }} className={'field'}>
          <span style={{ ...props.fieldDescriptionChildProps?.style }}>Side</span>
          <span>{props.side}</span>
        </div>
      </div>
    </div>
  );
};
