import { HTMLAttributes } from 'react';

export interface ICardDescriptionProps {
  gender: string;
  race: string;
  side: string;
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
  descriptionProps?: HTMLAttributes<HTMLDivElement>;
  fieldDescriptionProps?: HTMLAttributes<HTMLDivElement>;
  fieldDescriptionChildProps?: HTMLAttributes<HTMLSpanElement>;
}
