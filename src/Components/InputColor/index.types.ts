import { HTMLAttributes } from 'react';

export interface IInputColor {
  id: string;
  name?: string;
  value?: string;
  inputColorProps?: HTMLAttributes<HTMLInputElement>;
  onChangeInput?: {
    (e: React.ChangeEvent<HTMLInputElement>): void;
    <T = string | React.ChangeEvent<HTMLInputElement>>(field: T): T extends React.ChangeEvent<HTMLInputElement>
      ? void
      : (e: string | React.ChangeEvent<HTMLInputElement>) => void;
  };
}
