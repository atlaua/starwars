import { FC } from 'react';

import { IInputColor } from './index.types';
import './inputColor.css';

export const InputColor: FC<IInputColor> = (props): JSX.Element => {
  return (
    <input
      {...props.inputColorProps}
      style={{ ...props.inputColorProps?.style }}
      className={'inputColor'}
      id={props.id}
      name={props.name}
      type={'color'}
      value={props.value}
      onChange={props.onChangeInput}
    />
  );
};
