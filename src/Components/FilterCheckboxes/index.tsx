import { FC } from 'react';

import { IFilterCheckboxesProps } from './index.types';
import './filterCheckboxes.css';

export const FilterCheckboxes: FC<IFilterCheckboxesProps> = (props): JSX.Element => {
  return (
    <label {...props.labelProps} htmlFor={props.id} className={'label'} style={{ ...props.labelProps?.style }}>
      <input
        {...props.inputProps}
        id={props.id}
        className={'input-checkbox'}
        name={props.inputName}
        type="checkbox"
        defaultChecked={props.isChecked}
        onChange={(e): void => props.checkedInput?.(e)}
        style={{ ...props.inputProps?.style }}
      />
      <span
        {...props.fakeCheckboxProps}
        className={'fake-checkbox'}
        style={{ ...props.fakeCheckboxProps?.style }}
      ></span>
      {props.label}
    </label>
  );
};
