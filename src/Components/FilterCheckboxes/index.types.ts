import { HTMLAttributes } from 'react';

export interface IFilterCheckboxesProps {
  id: string;
  inputName: string;
  label: string;
  isChecked?: boolean;
  labelProps?: HTMLAttributes<HTMLLabelElement>;
  inputProps?: HTMLAttributes<HTMLInputElement>;
  fakeCheckboxProps?: HTMLAttributes<HTMLSpanElement>;
  checkedInput?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}
