import { HTMLAttributes } from 'react';

export interface IPaginationDot {
  id?: string;
  activeDot?: string;
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
}
