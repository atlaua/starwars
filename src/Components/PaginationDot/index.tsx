import { FC } from 'react';

import { IPaginationDot } from './index.types';
import './paginationDot.css';

export const PaginationDot: FC<IPaginationDot> = (props): JSX.Element => {
  return (
    <div
      {...props.wrapperProps}
      style={{ ...props.wrapperProps?.style }}
      className={`pagination__dot ${props.activeDot ? props.activeDot : ''}`}
      id={props.id}
    ></div>
  );
};
