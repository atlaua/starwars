import { FC } from 'react';

import { IInputComponent } from './index.types';
import './inputComponent.css';

export const Input: FC<IInputComponent> = (props): JSX.Element => {
  return (
    <div {...props.inputWrapperProps} style={{ ...props.inputWrapperProps?.style }}>
      <input
        {...props.inputProps}
        id={props.id}
        name={props.name}
        className={'input'}
        style={{
          borderColor: props.inputProps?.style?.borderColor ? props.inputProps?.style?.borderColor : '#ffffff',
          ...props.inputProps?.style,
        }}
        placeholder={props.placeholder}
        type={props.type}
        value={props.value}
        disabled={props.disabled}
        onChange={props.onChangeInput}
        onBlur={props.onBlurInput}
        autoComplete="off"
      />
      {props.icon ? (
        <img
          {...props.imageIconProps}
          className={'inputIcon'}
          src={props.icon}
          style={{ ...props.imageIconProps?.style }}
          alt={props.iconAlt}
        />
      ) : null}
    </div>
  );
};
