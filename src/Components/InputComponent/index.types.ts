import { HTMLAttributes } from 'react';

export interface IInputComponent {
  id: string;
  name?: string;
  inputWrapperProps?: HTMLAttributes<HTMLDivElement>;
  inputProps?: HTMLAttributes<HTMLInputElement>;
  imageIconProps?: HTMLAttributes<HTMLImageElement>;
  icon?: string;
  iconAlt?: string;
  placeholder?: string;
  value?: string;
  type?: string;
  disabled?: boolean;
  onChangeInput?: {
    (e: React.ChangeEvent<HTMLInputElement>): void;
    <T = string | React.ChangeEvent<HTMLInputElement>>(field: T): T extends React.ChangeEvent<HTMLInputElement>
      ? void
      : (e: string | React.ChangeEvent<HTMLInputElement>) => void;
  };
  onBlurInput?: {
    (e: React.FocusEvent<HTMLInputElement, Element>): void;
    <T = HTMLInputElement>(fieldOrEvent: T): T extends string ? (e: HTMLInputElement) => void : void;
  };
}
