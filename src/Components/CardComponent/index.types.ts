import { HTMLAttributes } from 'react';

export interface ICardComponent {
  id: string;
  name: string;
  avatar: string;
  cardWrapperProps?: HTMLAttributes<HTMLDivElement>;
  heroNameProps?: HTMLAttributes<HTMLDivElement>;
  avatarProps?: HTMLAttributes<HTMLDivElement>;
  handleClick?: (id: string) => void;
}
