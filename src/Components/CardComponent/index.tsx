import { FC } from 'react';

import { ICardComponent } from './index.types';
import './cardComponent.css';

export const Card: FC<ICardComponent> = (props): JSX.Element => {
  return (
    <div {...props.cardWrapperProps} className={'card'} onClick={(): void => props.handleClick?.(props.id)}>
      <div
        {...props.heroNameProps}
        className={'heroName'}
        style={{
          color: props.heroNameProps?.style?.color ? props.heroNameProps?.style?.color : 'white',
          ...props.heroNameProps?.style,
        }}
      >
        <span>{props.name}</span>
      </div>
      <div {...props.avatarProps} className={'avatarContainer'} style={{ ...props.avatarProps?.style }}>
        {props.avatar ? <img src={props.avatar} /> : null}
      </div>
      {props.children}
    </div>
  );
};
