import React, { FC } from 'react';

import { IPaginationComponent } from './index.types';
import './pagination.css';

export const Pagination: FC<IPaginationComponent> = (props): JSX.Element => {
  let arrayDots: number[] = [];
  const dotsCount = Math.ceil(props.totalElements / props.shownElementsOnPage);
  const maxDots = 3;

  for (let i = 0; i < dotsCount; i++) {
    if (i < maxDots) {
      arrayDots = [...arrayDots, i];
    }
  }

  const toggleActiveDot = (): number => {
    if (props.currentPage === 1) {
      //закрасить первую точку
      return 0;
    } else if (props.currentPage === Math.ceil(props.totalElements / props.shownElementsOnPage)) {
      // закрасить последнюю точку
      return arrayDots.length - 1;
    } else {
      // красить среднюю точку
      return 1;
    }
  };

  return (
    <div {...props.wrapperProps} style={{ ...props.wrapperProps?.style }} className={'pagination'}>
      {arrayDots.length !== 0 ? <div onClick={(): void => props.onClickBackArrow()}>{props.backArrow}</div> : null}
      {arrayDots.map((el) =>
        React.cloneElement(props.paginationDot, {
          key: el,
          id: el,
          activeDot: toggleActiveDot() === el ? 'activePage' : '',
        })
      )}
      {arrayDots.length !== 0 ? (
        <div onClick={(): void => props.onClickForwardArrow()}>{props.forwardArrow}</div>
      ) : null}
    </div>
  );
};
