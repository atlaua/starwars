import { HTMLAttributes } from 'react';

export interface IPaginationComponent {
  backArrow: React.ReactNode;
  forwardArrow: React.ReactNode;
  paginationDot: React.ReactElement;
  totalElements: number;
  shownElementsOnPage: number;
  currentPage: number;
  onClickBackArrow: () => void;
  onClickForwardArrow: () => void;
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
}
