import { FC, useEffect, useState } from 'react';

import { ISelectProps } from './index.types';

export const Select: FC<ISelectProps> = (props): JSX.Element => {
  const [isOpen, setIsOpen] = useState(props.isDropdownOpen);

  useEffect(() => setIsOpen(props.isDropdownOpen), [props.isDropdownOpen]);

  const clickOut = (e: MouseEvent): void => {
    const target = e.target as HTMLDivElement;
    if (!target.closest(`#dropdown_${props.dropdownId}`) && !target.closest(`#${props.idWrapper}`)) {
      props.setIsDropdownOpen(false);
    }
  };

  useEffect(() => {
    const element = document.getElementById(props.idElementForClick ?? '');

    element
      ? element.addEventListener('click', (e) => clickOut(e), true)
      : document.body.addEventListener('click', (e) => clickOut(e), true);

    return (): void => {
      element?.removeEventListener('click', (e) => clickOut(e), false);
    };
  }, []);

  return (
    <div>
      {props.label ? <label>{props.label}</label> : null}

      <div
        {...props.wrapperProps}
        id={props.idWrapper}
        style={{ ...props.wrapperProps?.style }}
        onClick={(e): void => props.handleClickWrapper(e)}
      >
        {props.select}
      </div>

      {isOpen && props.dropdown}
    </div>
  );
};
