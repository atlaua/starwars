import { HTMLAttributes } from 'react';

export interface ISelectProps {
  idWrapper: string;
  isDropdownOpen: boolean;
  select: React.ReactNode;
  setIsDropdownOpen: React.Dispatch<React.SetStateAction<boolean>>;
  dropdownId: string;
  dropdown: React.ReactNode;
  idElementForClick?: string;
  handleClickWrapper: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  label?: string;
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
}
