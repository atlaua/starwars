import { HTMLAttributes } from 'react';

export interface IModalProps {
  closeModal: () => void;
  overlayProps?: HTMLAttributes<HTMLDivElement>;
  containerProps?: HTMLAttributes<HTMLDivElement>;
}
