import { FC } from 'react';
import ReactDOM from 'react-dom';

import { IModalProps } from './index.types';
import './modal.css';

export const Modal: FC<IModalProps> = (props): React.ReactPortal | null => {
  const node = document.querySelector('#modal');
  if (!node) {
    return null;
  }

  return ReactDOM.createPortal(
    <div
      {...props.overlayProps}
      style={{ ...props.overlayProps?.style }}
      className={'overlay'}
      onClick={props.closeModal}
    >
      <div {...props.containerProps} style={{ ...props.containerProps?.style }} className={'modal__container'}>
        {props.children}
      </div>
    </div>,
    node
  );
};
