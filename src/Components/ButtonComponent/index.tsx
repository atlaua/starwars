import { FC } from 'react';

import { IButtonComponent } from './index.types';
import './buttonComponent.css';

export const ButtonComponent: FC<IButtonComponent> = (props): JSX.Element => {
  return (
    <button
      {...props.buttonProps}
      className={'btn'}
      style={{ ...props.buttonProps?.style }}
      type={props.type as 'button' | 'submit' | 'reset' | undefined}
      disabled={props.disabled}
      onClick={props.onClickBtn}
    >
      <div {...props.textProps} style={{ ...props.textProps?.style }}>
        {props.text}
      </div>
    </button>
  );
};
