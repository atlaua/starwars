import { HTMLAttributes } from 'react';

export interface IButtonComponent {
  text: string;
  type?: string;
  disabled?: boolean;
  buttonProps?: HTMLAttributes<HTMLButtonElement>;
  textProps?: HTMLAttributes<HTMLDivElement>;
  onClickBtn?: (e?: React.SyntheticEvent<any, Event> | undefined) => void;
}
