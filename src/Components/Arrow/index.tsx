import { FC } from 'react';

import { IArrow } from './index.types';

export const Arrow: FC<IArrow> = (props): JSX.Element => {
  return (
    <div {...props.wrapperProps}>
      {props.isForwardArrow ? (
        <img {...props.imgProps} style={{ cursor: 'pointer', ...props.imgProps?.style }} src={props.arrowForwardIcon} />
      ) : (
        <img
          {...props.imgProps}
          style={{ transform: 'rotate(180deg)', cursor: 'pointer', ...props.imgProps?.style }}
          src={props.arrowForwardIcon}
        />
      )}
    </div>
  );
};
