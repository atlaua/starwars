import { HTMLAttributes } from 'react';

export interface IArrow {
  isForwardArrow: boolean;
  arrowForwardIcon: string;
  wrapperProps?: HTMLAttributes<HTMLDivElement>;
  imgProps?: HTMLAttributes<HTMLImageElement>;
}
