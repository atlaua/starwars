import { ISelectContent } from '../Core/Domain/interfaces/ISelectContent';
import { IParametersModel } from '../services/parameters/models/IParametersModel';

export const addCurrentFilterCheckboxState = (
  currentFilterValues: IParametersModel[],
  currentParameterState: string[]
): ISelectContent[] => {
  const initialValue = currentFilterValues.map((el) => {
    if (currentParameterState.includes(el.id)) {
      return { ...el, isChecked: true };
    }
    return { ...el, isChecked: false };
  });

  return initialValue;
};
