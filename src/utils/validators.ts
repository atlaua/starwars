import { IAddForm } from '../Core/Domain/interfaces/IAddForm';

export interface IFormErrors {
  heroName: string;
  addGender: string;
  addRace: string;
  addSide: string;
  addDescription: string;
  tags: string;
  photoInputFile: string;
  colorName: string;
  backgroundColor: string;
  parametersColor: string;
}

export function validate(values: IAddForm): Partial<IFormErrors> {
  const errors: Partial<IFormErrors> = {};

  if (!values.heroName) {
    errors.heroName = 'Name is required';
  }

  if (!values.addGender.value) {
    errors.addGender = 'Gender is required';
  }

  if (!values.addRace.value) {
    errors.addRace = 'Race is required';
  }

  if (!values.addSide.value) {
    errors.addSide = 'Side is required';
  }

  if (!values.addDescription) {
    errors.addDescription = 'Description is required';
  } else if (values.addDescription.length > 100) {
    errors.addDescription = 'Rich max length';
  }

  const re: RegExp = /,[ ,]/;
  const maxLength = 3;
  const tagList: string[] = values.tags.split(re);
  if (!values.tags) {
    errors.tags = 'Tags is required';
  } else if (tagList.length > maxLength) {
    errors.tags = 'Rich max length';
  }

  if (!values.photoInputFile) {
    errors.photoInputFile = 'Photo is required';
  }

  return errors;
}
